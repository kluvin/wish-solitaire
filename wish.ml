open Containers

module Deck = Map.Make(String) [@@deriving show]

type value = Seven | Eight | Nine | Ten | Jack | Queen | King | Ace [@@deriving show]
type suit = Spades | Diamond | Hearts | Clubs [@@deriving show]
type card = value * suit [@@deriving show]
type stack = card list [@@deriving show]
type t = stack Deck.t
type flat_t = (string * stack) list [@@deriving show]
let pp deck = pp_flat_t Fmt.stdout (Deck.bindings deck)

let tags = [ "A" ; "B" ; "C" ; "D" ; "E" ; "F" ; "G" ; "H" ]

let shuffle d =
    let d = Array.of_list d in
    Array.shuffle d;
    Array.to_list d

let make_deck ?shuffled:(shuffled=false) () =
  let suits  = [ Spades ; Diamond ; Hearts ; Clubs ]
  and values = [ Seven ; Eight ; Nine ; Ten
               ; Jack ; Queen ; King ; Ace ]
  and tuple a b = (a , b)
  and id x = x
  in
    List.product tuple values suits
      |> (fun x -> if shuffled then shuffle x else id x)
      |> List.chunks 4
      |> List.combine tags
      |> Deck.of_list

let remove a b deck =
  let a_stack = Deck.find a deck
  and b_stack = Deck.find b deck
  in
    match a_stack, b_stack with
      (* Equal heads *)
      | (hd :: tl) , (hd' :: tl') when Equal.poly (fst hd) (fst hd') ->
        Some (Deck.add_list deck [ (a , tl) ; (b , tl') ])
      (* Either stack is empty *)
      | a , b when List.(exists is_empty [ a ; b ]) -> None
      (* Can't happen *)
      | _ -> None

let top_value stack =
  match snd stack with
  | [] -> 0
  | (value, _) :: _ -> match value with
    | Seven -> 7
    | Eight -> 8
    | Nine -> 9
    | Ten -> 10
    | Jack -> 11
    | Queen -> 12
    | King -> 13
    | Ace -> 14

let move deck =
  Deck.bindings deck
    |> List.filter (fun l -> (top_value l) <> 0)
    |> List.group_by
        ~hash:top_value
        ~eq:(fun a b -> Equal.poly (top_value a) (top_value b))
    |> List.sort List.compare_lengths
    |> List.rev
    |> function
      | [] -> None
      | hd :: _ -> match hd with
        | [] | [_] -> None
        | (tag , _) :: (tag' , _) :: _ -> Some (tag , tag')


let play () =
  let rec loop deck =
    match move deck with
      | None -> deck
      | Some (a , b) -> match (remove a b deck) with
        | None -> deck
        | Some new_deck -> loop new_deck
  in
  loop (make_deck ~shuffled:true ())

let () =
  Random.self_init ();
  pp (play ())
