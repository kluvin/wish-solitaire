
from typing import *
from utils import *
from itertools import product, groupby, starmap
from random import sample
from dataclasses import dataclass

suits  = ["Spades", "Diamond", "Hearts",  "Clubs"]
values = ["7", "8", "9", "10", "Jack", "Queen", "King", "Ace"]
tags   = ["A", "B", "C", "D", "E", "F", "G", "H"]

@dataclass
class Card:
    value: str
    suit: str

Deck = dict[str, list[Card]]


def io_main():
    """Top-level game loop
    """
    deck = make_deck()
    empty_stacks = [stack for _, stack in deck.items() if len(stack) == 0]
    
    done = len(empty_stacks) == 4
    has_duplicates = filter(lambda x: len(x) >= 2, groups(deck))
    while has_duplicates and not done:
        io_show(deck)
        picked_stacks = io_read_next_move()
        deck = move(*picked_stacks, deck)


def make_deck(shuffled:bool=True):
    """Generate a deck of stacks of Cards.
    Optionally shuffled.
    """
    return pipe((),
        lambda _: product(values, suits),
        lambda x: sample(x, k=len(x)) if shuffled else id,
        lambda x: starmap(Card, x),
        lambda x: chunks(4, x),
        lambda x: zip(tags, x),
        lambda x: dict(x),
    )

def move(a:str, b:str, deck:Deck) -> Union[Deck, None]:
    """Definition of a move.
    """
    same_top = hd(deck[a]).value == hd(deck[b]).value
    no_empty_list = any([deck[a], deck[b]])
    if same_top and no_empty_list:
        deck[a] = tl(deck[a])
        deck[b] = tl(deck[b])
        return deck
    return None


def io_show(deck:Deck):
    """Print the game state to screen
    """
    io_clear_screen()

    deck = deck.items()
    sizes = [len(stack) for _, stack in deck]
    for line, size in zip(fmt_top_values(deck), sizes):
        print(f'{line} | +{size - 1} more cards', end="\n")
    print()


def io_clear_screen():
    print(chr(27) + "[2J")


def io_read_next_move() -> tuple[str, str]:
    return tuple(input("Pick two stacks (e.g.: X Y)\n: ").split())


def groups(deck:Deck) -> list[tuple[str, list]]:
    """Deck of cards to groups of same values: e.g., [[x, x], [y], [k, k]]
    """
    top_cards = [hd(stack).value for _, stack in deck.items()]
    return [group for _, group in groupby(sorted(top_cards))]


def io_save(deck:Deck):
    with open('wish.o', 'wb') as f:
        json.dump(deck, f)


def io_load():
    with open('wish.o', 'rb') as f:
        return json.load(f)


def fmt_top_values(deck:Deck) -> list[str]:
    """Formatted lines describing the stacks and their top value"""
    lines = [f'{k}: {hd(s).value} of {hd(s).suit}' for k, s in deck]
    max_len = max([len(x) for x in lines])
    lines = [str.ljust(line, max_len + 2) for line in lines]
    return lines


io_main()
