include Wish

let test_make_deck_len () =
  Alcotest.(check bool) "Makes a deck" true 
  (Wish.make_deck
    |> Wish.Deck.bindings
    |> List.map snd
    |> List.flatten
    |> (fun l -> (List.length l) = 32))

let test_first_move () =
  Alcotest.(check bool) "First move is OK"
  (Wish.make_deck
  |> Wish.move)

let () = 
  let open Alcotest in
  run "Wish solitaire"
    [
      "make-deck", [
        test_case "Length is 32" `Quick test_make_deck_len
      ];
      "move", [
        test_case "Valid move when empty stack" `Quick test_first_move
      ]
    ]
