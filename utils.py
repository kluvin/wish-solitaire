from pprint import pp
from typing import *
T = TypeVar('T')


def hd(x:list[T]) -> T:
    """Head of x such that `hd([1, 2, 3]) = 1`
    """
    return x[0]


def tl(x:list[T]) -> list[T]:
    """Tail of x such that `tl([1, 2, 3]) = [2, 3]`
    """
    return x[1:]


def pipe(data, *funcs:list[Callable]):
    """Right associative function composition such that:
    `pipe(x, f, g)` = `g(f(x))` -- stolen from functoolz
    """
    for func in funcs:
        data = func(list(data))
    return data


def chunks(n:int, lst:list[T]) -> list[list[T]]:
    """Sublists of size n
    """
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


id = lambda x: x
def log(x, printer=id):
    """Logging with optional printer
    """
    pp(printer(x))
    return x
