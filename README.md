
# Rules

Sourced from [semicolon.com](https://www.semicolon.com/Solitaire/Rules/TheWish.html).

## Layout
Remove all Twos, Threes, Fours, Fives, and Sixes from your deck and discard them, keeping the Aces and the Sevens through Kings. Shuffle those cards and lay them out in eight tableau piles of four cards each, face up and squared.

## Play
Pairs of cards that match in rank (such as two Sevens or two Kings) may be discarded. (In Solitaire Till Dawn, drag a card and drop it on a matching card, and both will be discarded automatically.)

## Goal
The goal is to discard all 32 cards.

## Tips
There are two pairs for every rank: that is, two pairs of Kings, two pairs of sevens, and so on. Try to remember which ones you have already discarded. If you have already discarded one pair of Kings, then it is safe to discard the other pair. If not, you might want to wait and see whether you should pair them up differently.

Empty piles are useless. Don’t discard the last card in a pile until you have no other choice.

# Implementations

Missing feature parity. Python is implemented with a small GUI that will crash on game completion or invalid input. OCaml is implemented with a solver and no UI thus far. Should aim for feature parity, and tests for both.

## wish.ml

The interesting bit. Here is questions for review:

* L24-25 -- No generic tuple and id?
* L39-40 -- Important logic. Possible to simplify it a bit?
* L42 -- Guarded clause is used instead of manually checking `[] , [] | [_] , [] | [] , [_]`. Is there a compromise?
* L44 -- Comes as a consequence of the above, of course. Though, if it was instead possible to get rid of this that is OK too.
* L49-57 -- Automatic integer serialization of custom types? Order does not matter.
* L76-81 -- This one is stressing me a bit because when `move` returns `Some` we know that `remove` should succeed, and so the check for `None` is redundant here. 

I am not sure if I shouldn't let `tags` be custom types as well. Although it seems that would be for not much benefit.

Another problem is that I have defined some types for my data, but the type annotations in my IDE are generic, such that I get `(value * suit)` instead of the expected `card`, this is even after I have written type annotations in my `.mli`.

Yet more that would be really useful in hindsight is to have a better idea of what's going on inside my program as I write it. Especially with respect to `move` and `remove`. Writing up tests would be one approach, another would be to abuse the REPL to show me what the data looks like as it is transformed in a pipeline, but I am not able to find out how to do this ergonomically.

With respect to writing up tests, I struggle a bit figuring out how to effectively test `make_deck`. In a dynamic language I would stringify the result of `make_deck` once to any representation the language gives me, and then use that as my test. The 'proper' way would of course be to realize that a flattened deck of 4 * 8 suits and values in any order is a good representation, though at that point I am reimplementing much of the logic I am testing.



## wish.py

One obvious thing that is missing here is that it would be nice to have partial application in `make_deck`.