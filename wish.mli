module Deck :
  sig
    type key = string
    type 'a t = 'a Map.Make(Containers.String).t
    val empty : 'a t
    val is_empty : 'a t -> bool
    val mem : key -> 'a t -> bool
    val add : key -> 'a -> 'a t -> 'a t
    val singleton : key -> 'a -> 'a t
    val remove : key -> 'a t -> 'a t
    val merge :
      (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
    val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
    val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
    val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
    val iter : (key -> 'a -> unit) -> 'a t -> unit
    val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val for_all : (key -> 'a -> bool) -> 'a t -> bool
    val exists : (key -> 'a -> bool) -> 'a t -> bool
    val filter : (key -> 'a -> bool) -> 'a t -> 'a t
    val filter_map : (key -> 'a -> 'b option) -> 'a t -> 'b t
    val partition : (key -> 'a -> bool) -> 'a t -> 'a t * 'a t
    val cardinal : 'a t -> int
    val bindings : 'a t -> (key * 'a) list
    val min_binding : 'a t -> key * 'a
    val max_binding : 'a t -> key * 'a
    val choose : 'a t -> key * 'a
    val split : key -> 'a t -> 'a t * 'a option * 'a t
    val find : key -> 'a t -> 'a
    val find_last : (key -> bool) -> 'a t -> key * 'a
    val find_last_opt : (key -> bool) -> 'a t -> (key * 'a) option
    val map : ('a -> 'b) -> 'a t -> 'b t
    val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
    val to_seq : 'a t -> (key * 'a) Seq.t
    val to_seq_from : key -> 'a t -> (key * 'a) Seq.t
    val get : key -> 'a t -> 'a option
    val get_or : key -> 'a t -> default:'a -> 'a
    val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
    val choose_opt : 'a t -> (key * 'a) option
    val min_binding_opt : 'a t -> (key * 'a) option
    val max_binding_opt : 'a t -> (key * 'a) option
    val find_opt : key -> 'a t -> 'a option
    val find_first : (key -> bool) -> 'a t -> key * 'a
    val find_first_opt : (key -> bool) -> 'a t -> (key * 'a) option
    val merge_safe :
      f:(key ->
         [ `Both of 'a * 'b | `Left of 'a | `Right of 'b ] -> 'c option) ->
      'a t -> 'b t -> 'c t
    val add_seq : 'a t -> (key * 'a) Seq.t -> 'a t
    val add_seq_with :
      f:(key -> 'a -> 'a -> 'a) -> 'a t -> (key * 'a) Seq.t -> 'a t
    val of_seq : (key * 'a) Seq.t -> 'a t
    val of_seq_with : f:(key -> 'a -> 'a -> 'a) -> (key * 'a) Seq.t -> 'a t
    val add_iter : 'a t -> (key * 'a) CCMap.iter -> 'a t
    val add_iter_with :
      f:(key -> 'a -> 'a -> 'a) -> 'a t -> (key * 'a) CCMap.iter -> 'a t
    val of_iter : (key * 'a) CCMap.iter -> 'a t
    val of_iter_with :
      f:(key -> 'a -> 'a -> 'a) -> (key * 'a) CCMap.iter -> 'a t
    val to_iter : 'a t -> (key * 'a) CCMap.iter
    val of_list : (key * 'a) list -> 'a t
    val of_list_with : f:(key -> 'a -> 'a -> 'a) -> (key * 'a) list -> 'a t
    val add_list : 'a t -> (key * 'a) list -> 'a t
    val add_list_with :
      f:(key -> 'a -> 'a -> 'a) -> 'a t -> (key * 'a) list -> 'a t
    val keys : 'a t -> key CCMap.iter
    val values : 'a t -> 'a CCMap.iter
    val to_list : 'a t -> (key * 'a) list
    val pp :
      ?pp_start:unit CCMap.printer ->
      ?pp_stop:unit CCMap.printer ->
      ?pp_arrow:unit CCMap.printer ->
      ?pp_sep:unit CCMap.printer ->
      key CCMap.printer -> 'a CCMap.printer -> 'a t CCMap.printer
  end
type value = Seven | Eight | Nine | Ten | Jack | Queen | King | Ace
val pp_value : Format.formatter -> value -> unit
val show_value : value -> string
type suit = Spades | Diamond | Hearts | Clubs
val pp_suit : Format.formatter -> suit -> unit
val show_suit : suit -> string
type card = value * suit
val pp_card : Format.formatter -> card -> unit
val show_card : card -> string
type stack = card list
val pp_stack : Format.formatter -> stack -> unit
val show_stack : stack -> string
type t = stack Deck.t
type flat_t = (string * stack) list
val pp_flat_t : Format.formatter -> flat_t -> unit
val show_flat_t : flat_t -> string
val pp : stack Deck.t -> unit
val tags : string list
val shuffle : 'a list -> 'a list
val make_deck : ?shuffled:bool -> unit -> t
val remove :
  string -> string -> t -> t option
val top_value : string * stack -> int
val move : t -> (string * string) option
val play : unit -> t
